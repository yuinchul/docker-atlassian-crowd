--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.13
-- Dumped by pg_dump version 9.5.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cwd_app_dir_default_groups; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_app_dir_default_groups (
    id bigint NOT NULL,
    application_mapping_id bigint NOT NULL,
    group_name character varying(255) NOT NULL
);


ALTER TABLE public.cwd_app_dir_default_groups OWNER TO crowduser;

--
-- Name: cwd_app_dir_group_mapping; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_app_dir_group_mapping (
    id bigint NOT NULL,
    app_dir_mapping_id bigint NOT NULL,
    application_id bigint NOT NULL,
    directory_id bigint NOT NULL,
    group_name character varying(255) NOT NULL
);


ALTER TABLE public.cwd_app_dir_group_mapping OWNER TO crowduser;

--
-- Name: cwd_app_dir_mapping; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_app_dir_mapping (
    id bigint NOT NULL,
    application_id bigint NOT NULL,
    directory_id bigint NOT NULL,
    allow_all character(1) NOT NULL,
    list_index integer
);


ALTER TABLE public.cwd_app_dir_mapping OWNER TO crowduser;

--
-- Name: cwd_app_dir_operation; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_app_dir_operation (
    app_dir_mapping_id bigint NOT NULL,
    operation_type character varying(32) NOT NULL
);


ALTER TABLE public.cwd_app_dir_operation OWNER TO crowduser;

--
-- Name: cwd_application; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_application (
    id bigint NOT NULL,
    application_name character varying(255) NOT NULL,
    lower_application_name character varying(255) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL,
    active character(1) NOT NULL,
    description character varying(255),
    application_type character varying(32) NOT NULL,
    credential character varying(255) NOT NULL
);


ALTER TABLE public.cwd_application OWNER TO crowduser;

--
-- Name: cwd_application_address; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_application_address (
    application_id bigint NOT NULL,
    remote_address character varying(255) NOT NULL
);


ALTER TABLE public.cwd_application_address OWNER TO crowduser;

--
-- Name: cwd_application_alias; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_application_alias (
    id bigint NOT NULL,
    application_id bigint NOT NULL,
    user_name character varying(255) NOT NULL,
    lower_user_name character varying(255) NOT NULL,
    alias_name character varying(255) NOT NULL,
    lower_alias_name character varying(255) NOT NULL
);


ALTER TABLE public.cwd_application_alias OWNER TO crowduser;

--
-- Name: cwd_application_attribute; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_application_attribute (
    application_id bigint NOT NULL,
    attribute_name character varying(255) NOT NULL,
    attribute_value character varying(4000)
);


ALTER TABLE public.cwd_application_attribute OWNER TO crowduser;

--
-- Name: cwd_audit_log_changeset; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_audit_log_changeset (
    id bigint NOT NULL,
    audit_timestamp bigint NOT NULL,
    author_type character varying(255) NOT NULL,
    author_id bigint,
    author_name character varying(255),
    event_type character varying(255) NOT NULL,
    ip_address character varying(45),
    event_message character varying(255),
    event_source character varying(255) NOT NULL
);


ALTER TABLE public.cwd_audit_log_changeset OWNER TO crowduser;

--
-- Name: cwd_audit_log_entity; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_audit_log_entity (
    id bigint NOT NULL,
    entity_type character varying(255) NOT NULL,
    entity_id bigint,
    entity_name character varying(255),
    is_primary character(1) NOT NULL,
    changeset_id bigint NOT NULL
);


ALTER TABLE public.cwd_audit_log_entity OWNER TO crowduser;

--
-- Name: cwd_audit_log_entry; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_audit_log_entry (
    id bigint NOT NULL,
    property_name character varying(255) NOT NULL,
    old_value character varying(4000),
    new_value character varying(4000),
    changeset_id bigint NOT NULL
);


ALTER TABLE public.cwd_audit_log_entry OWNER TO crowduser;

--
-- Name: cwd_cluster_heartbeat; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_cluster_heartbeat (
    node_id character varying(36) NOT NULL,
    node_name character varying(255),
    hearbeat_timestamp bigint NOT NULL
);


ALTER TABLE public.cwd_cluster_heartbeat OWNER TO crowduser;

--
-- Name: cwd_cluster_info; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_cluster_info (
    node_id character varying(255) NOT NULL,
    ip_address character varying(255),
    hostname character varying(255),
    current_heap bigint,
    max_heap bigint,
    load_average double precision,
    uptime bigint,
    info_timestamp bigint NOT NULL
);


ALTER TABLE public.cwd_cluster_info OWNER TO crowduser;

--
-- Name: cwd_cluster_job; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_cluster_job (
    id character varying(255) NOT NULL,
    runner_key character varying(255) NOT NULL,
    job_interval bigint,
    cron_expression character varying(120),
    time_zone character varying(80),
    next_run_timestamp bigint,
    version bigint NOT NULL,
    job_parameters bytea,
    claim_node_id character varying(36),
    claim_timestamp bigint
);


ALTER TABLE public.cwd_cluster_job OWNER TO crowduser;

--
-- Name: cwd_cluster_lock; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_cluster_lock (
    lock_name character varying(255) NOT NULL,
    lock_timestamp bigint,
    node_id character varying(36)
);


ALTER TABLE public.cwd_cluster_lock OWNER TO crowduser;

--
-- Name: cwd_cluster_message; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_cluster_message (
    id bigint NOT NULL,
    channel character varying(64),
    msg_text character varying(1024),
    msg_timestamp bigint,
    sender_node_id character varying(36)
);


ALTER TABLE public.cwd_cluster_message OWNER TO crowduser;

--
-- Name: cwd_cluster_message_id; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_cluster_message_id (
    next_val bigint
);


ALTER TABLE public.cwd_cluster_message_id OWNER TO crowduser;

--
-- Name: cwd_cluster_safety; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_cluster_safety (
    entry_key character varying(255) NOT NULL,
    entry_value character varying(255),
    node_id character varying(36),
    ip_address character varying(255),
    entry_timestamp bigint NOT NULL
);


ALTER TABLE public.cwd_cluster_safety OWNER TO crowduser;

--
-- Name: cwd_databasechangelog; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.cwd_databasechangelog OWNER TO crowduser;

--
-- Name: cwd_databasechangeloglock; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.cwd_databasechangeloglock OWNER TO crowduser;

--
-- Name: cwd_directory; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_directory (
    id bigint NOT NULL,
    directory_name character varying(255) NOT NULL,
    lower_directory_name character varying(255) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL,
    active character(1) NOT NULL,
    description character varying(255),
    impl_class character varying(255) NOT NULL,
    lower_impl_class character varying(255) NOT NULL,
    directory_type character varying(32) NOT NULL
);


ALTER TABLE public.cwd_directory OWNER TO crowduser;

--
-- Name: cwd_directory_attribute; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_directory_attribute (
    directory_id bigint NOT NULL,
    attribute_name character varying(255) NOT NULL,
    attribute_value character varying(4000)
);


ALTER TABLE public.cwd_directory_attribute OWNER TO crowduser;

--
-- Name: cwd_directory_operation; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_directory_operation (
    directory_id bigint NOT NULL,
    operation_type character varying(32) NOT NULL
);


ALTER TABLE public.cwd_directory_operation OWNER TO crowduser;

--
-- Name: cwd_expirable_user_token; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_expirable_user_token (
    id bigint NOT NULL,
    token character varying(255) NOT NULL,
    user_name character varying(255),
    email_address character varying(255),
    expiry_date bigint NOT NULL,
    directory_id bigint NOT NULL
);


ALTER TABLE public.cwd_expirable_user_token OWNER TO crowduser;

--
-- Name: cwd_granted_perm; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_granted_perm (
    id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL,
    permission_id integer NOT NULL,
    app_dir_mapping_id bigint NOT NULL,
    group_name character varying(255) NOT NULL
);


ALTER TABLE public.cwd_granted_perm OWNER TO crowduser;

--
-- Name: cwd_group; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_group (
    id bigint NOT NULL,
    group_name character varying(255) NOT NULL,
    lower_group_name character varying(255) NOT NULL,
    active character(1) NOT NULL,
    is_local character(1) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL,
    description character varying(255),
    group_type character varying(32) NOT NULL,
    directory_id bigint NOT NULL,
    external_id character varying(255)
);


ALTER TABLE public.cwd_group OWNER TO crowduser;

--
-- Name: cwd_group_attribute; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_group_attribute (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    directory_id bigint NOT NULL,
    attribute_name character varying(255) NOT NULL,
    attribute_value character varying(255),
    attribute_lower_value character varying(255)
);


ALTER TABLE public.cwd_group_attribute OWNER TO crowduser;

--
-- Name: cwd_membership; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_membership (
    id bigint NOT NULL,
    parent_id bigint,
    child_id bigint,
    membership_type character varying(32),
    group_type character varying(32) NOT NULL,
    parent_name character varying(255) NOT NULL,
    lower_parent_name character varying(255) NOT NULL,
    child_name character varying(255) NOT NULL,
    lower_child_name character varying(255) NOT NULL,
    created_date timestamp without time zone,
    directory_id bigint NOT NULL
);


ALTER TABLE public.cwd_membership OWNER TO crowduser;

--
-- Name: cwd_property; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_property (
    property_key character varying(255) NOT NULL,
    property_name character varying(255) NOT NULL,
    property_value character varying(4000)
);


ALTER TABLE public.cwd_property OWNER TO crowduser;

--
-- Name: cwd_synchronisation_status; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_synchronisation_status (
    id integer NOT NULL,
    directory_id bigint NOT NULL,
    node_id character varying(36),
    sync_start bigint NOT NULL,
    sync_end bigint,
    sync_status character varying(255),
    status_parameters text
);


ALTER TABLE public.cwd_synchronisation_status OWNER TO crowduser;

--
-- Name: cwd_synchronisation_token; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_synchronisation_token (
    directory_id bigint NOT NULL,
    sync_status_token text
);


ALTER TABLE public.cwd_synchronisation_token OWNER TO crowduser;

--
-- Name: cwd_token; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_token (
    id bigint NOT NULL,
    directory_id bigint NOT NULL,
    entity_name character varying(255) NOT NULL,
    random_number bigint NOT NULL,
    identifier_hash character varying(255) NOT NULL,
    random_hash character varying(255) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    last_accessed_date timestamp without time zone NOT NULL,
    last_accessed_time bigint DEFAULT 0 NOT NULL,
    duration bigint
);


ALTER TABLE public.cwd_token OWNER TO crowduser;

--
-- Name: cwd_tombstone; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_tombstone (
    id bigint NOT NULL,
    tombstone_type character varying(255) NOT NULL,
    tombstone_timestamp bigint NOT NULL,
    entity_name character varying(255),
    directory_id bigint,
    parent character varying(255),
    application_id bigint
);


ALTER TABLE public.cwd_tombstone OWNER TO crowduser;

--
-- Name: cwd_user; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_user (
    id bigint NOT NULL,
    user_name character varying(255) NOT NULL,
    lower_user_name character varying(255) NOT NULL,
    active character(1) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL,
    first_name character varying(255),
    lower_first_name character varying(255),
    last_name character varying(255),
    lower_last_name character varying(255),
    display_name character varying(255),
    lower_display_name character varying(255),
    email_address character varying(255),
    lower_email_address character varying(255),
    external_id character varying(255),
    directory_id bigint NOT NULL,
    credential character varying(255)
);


ALTER TABLE public.cwd_user OWNER TO crowduser;

--
-- Name: cwd_user_attribute; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_user_attribute (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    directory_id bigint NOT NULL,
    attribute_name character varying(255) NOT NULL,
    attribute_value character varying(255),
    attribute_lower_value character varying(255)
);


ALTER TABLE public.cwd_user_attribute OWNER TO crowduser;

--
-- Name: cwd_user_credential_record; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_user_credential_record (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    password_hash character varying(255) NOT NULL,
    list_index integer
);


ALTER TABLE public.cwd_user_credential_record OWNER TO crowduser;

--
-- Name: cwd_webhook; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.cwd_webhook (
    id bigint NOT NULL,
    endpoint_url character varying(255) NOT NULL,
    application_id bigint NOT NULL,
    token character varying(255),
    oldest_failure_date timestamp without time zone,
    failures_since_last_success bigint NOT NULL
);


ALTER TABLE public.cwd_webhook OWNER TO crowduser;

--
-- Name: hibernate_unique_key; Type: TABLE; Schema: public; Owner: crowduser
--

CREATE TABLE public.hibernate_unique_key (
    next_hi integer
);


ALTER TABLE public.hibernate_unique_key OWNER TO crowduser;

--
-- Data for Name: cwd_app_dir_default_groups; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_app_dir_default_groups (id, application_mapping_id, group_name) FROM stdin;
\.


--
-- Data for Name: cwd_app_dir_group_mapping; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) FROM stdin;
360449	327681	32770	163841	crowd-administrators
\.


--
-- Data for Name: cwd_app_dir_mapping; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) FROM stdin;
327681	32770	163841	F	0
\.


--
-- Data for Name: cwd_app_dir_operation; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) FROM stdin;
327681	UPDATE_USER_ATTRIBUTE
327681	DELETE_ROLE
327681	UPDATE_GROUP
327681	DELETE_USER
327681	CREATE_ROLE
327681	CREATE_GROUP
327681	UPDATE_ROLE
327681	CREATE_USER
327681	UPDATE_USER
327681	DELETE_GROUP
327681	UPDATE_ROLE_ATTRIBUTE
327681	UPDATE_GROUP_ATTRIBUTE
\.


--
-- Data for Name: cwd_application; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) FROM stdin;
32769	google-apps	google-apps	2018-08-09 05:41:51.457	2018-08-09 05:42:55.261	T	Google Applications Connector	PLUGIN	{PKCS5S2}iRnlxI3rm90I87NMGsrbYTkSbPnttvRO0aC1xFEG/N84dj8askmfNkFTMfddo1wT
32770	crowd	crowd	2018-08-09 05:42:52.156	2018-08-09 05:42:55.286	T	Crowd console	CROWD	{PKCS5S2}D0h7tyRiaL1z6ENN0GxsaBmOiZS5uG6IXdzwwvC7z5QzGePS/IQBKY/aqkRqPsOg
\.


--
-- Data for Name: cwd_application_address; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_application_address (application_id, remote_address) FROM stdin;
\.


--
-- Data for Name: cwd_application_alias; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_application_alias (id, application_id, user_name, lower_user_name, alias_name, lower_alias_name) FROM stdin;
\.


--
-- Data for Name: cwd_application_attribute; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_application_attribute (application_id, attribute_name, attribute_value) FROM stdin;
32769	atlassian_sha1_applied	true
32770	atlassian_sha1_applied	true
32770	aggregateMemberships	false
32770	lowerCaseOutput	false
32770	aliasingEnabled	false
32769	aggregateMemberships	false
32769	lowerCaseOutput	false
32769	aliasingEnabled	false
\.


--
-- Data for Name: cwd_audit_log_changeset; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_audit_log_changeset (id, audit_timestamp, author_type, author_id, author_name, event_type, ip_address, event_message, event_source) FROM stdin;
65537	1533793312315	OTHER	\N		APPLICATION_CREATED	172.17.0.1		MANUAL
65538	1533793320743	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65539	1533793320749	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65540	1533793320753	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65541	1533793320760	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65542	1533793320764	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65543	1533793320767	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65544	1533793320772	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65545	1533793320776	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65546	1533793350165	OTHER	\N		DIRECTORY_CREATED	172.17.0.1		MANUAL
65547	1533793372059	OTHER	\N		USER_CREATED	172.17.0.1		MANUAL
65548	1533793372115	OTHER	\N		GROUP_CREATED	172.17.0.1		MANUAL
65549	1533793372133	OTHER	\N		ADDED_TO_GROUP	172.17.0.1		MANUAL
65550	1533793372219	OTHER	\N		APPLICATION_CREATED	172.17.0.1		MANUAL
65551	1533793372239	OTHER	\N		APPLICATION_UPDATED	172.17.0.1		MANUAL
65552	1533793375173	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65553	1533793375181	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65554	1533793375185	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65555	1533793375189	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65556	1533793375192	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65557	1533793375197	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
65558	1533793375273	OTHER	\N		APPLICATION_UPDATED	172.17.0.1		MANUAL
65559	1533793375378	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
425985	1533793375478	OTHER	\N		CONFIGURATION_MODIFIED	172.17.0.1		MANUAL
\.


--
-- Data for Name: cwd_audit_log_entity; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_audit_log_entity (id, entity_type, entity_id, entity_name, is_primary, changeset_id) FROM stdin;
131073	APPLICATION	32769	google-apps	T	65537
131074	DIRECTORY	163841	MOUSoft Inc. Crowd server	T	65546
131075	USER	196609	crowdadmin	T	65547
131076	DIRECTORY	163841	MOUSoft Inc. Crowd server	F	65547
131077	DIRECTORY	163841	MOUSoft Inc. Crowd server	F	65548
131078	GROUP	262145	crowd-administrators	T	65548
131079	USER	196609	crowdadmin	F	65549
131080	DIRECTORY	163841	MOUSoft Inc. Crowd server	F	65549
131081	GROUP	262145	crowd-administrators	T	65549
131082	APPLICATION	32770	crowd	T	65550
131083	APPLICATION	32770	crowd	T	65551
131084	APPLICATION	32769	google-apps	T	65558
\.


--
-- Data for Name: cwd_audit_log_entry; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_audit_log_entry (id, property_name, old_value, new_value, changeset_id) FROM stdin;
98305	Type		PLUGIN	65537
98306	Name		google-apps	65537
98307	Description		Google Applications Connector	65537
98308	Attribute: atlassian_sha1_applied		true	65537
98309	Created Date		2018-08-09T05:41:51.457+0000	65537
98310	Is permanent		true	65537
98311	Active		true	65537
98312	Password		*****	65537
98313	deployment.title		MOUSoft Inc. Crowd server	65538
98314	des.encryption.key		*****	65539
98315	session.time		1800000	65540
98316	cache.enabled	false	true	65541
98317	base.url		http://localhost:8099/crowd	65542
98318	mailserver.message.template		Hello $firstname $lastname,\n\nYou (or someone else) have requested to reset your password for $deploymenttitle on $date.\n\nIf you follow the link below you will be able to personally reset your password.\n$resetlink\n\nThis password reset request is valid for the next 24 hours.\n\nHere are the details of your account:\n\nUsername: $username\nFull name: $firstname $lastname\nYour account is currently: $active\n\n$deploymenttitle administrator	65543
98319	email.template.forgotten.username		Hello $firstname $lastname,\n\nYou have requested the username for your email address: $email.\n\nYour username(s) are: $username\n\nIf you think this email was sent incorrectly, please contact one of the administrators at: $admincontact\n\n$deploymenttitle administrator	65544
98320	mailserver.prefix		[MOUSoft Inc. Crowd server - Atlassian Crowd]	65545
98321	Created date		2018-08-09T05:42:30.117+0000	65546
98322	Attribute: password_complexity_message			65546
98323	Attribute: password_regex			65546
98324	Description			65546
98325	Attribute: user_encryption_method		atlassian-security	65546
98326	Encryption type		atlassian-security	65546
98327	Allowed operations	[]	[CREATE_GROUP, CREATE_USER, DELETE_GROUP, DELETE_USER, UPDATE_GROUP, UPDATE_GROUP_ATTRIBUTE, UPDATE_USER, UPDATE_USER_ATTRIBUTE]	65546
98328	Directory type		INTERNAL	65546
98329	Name		MOUSoft Inc. Crowd server	65546
98330	Attribute: password_max_change_time		0	65546
98331	Attribute: password_max_attempts		0	65546
98332	Attribute: password_history_count		0	65546
98333	Active		true	65546
98334	Implementation class		com.atlassian.crowd.directory.InternalDirectory	65546
98335	Last name		administrator	65547
98336	First name		crowd	65547
98337	Username		crowdadmin	65547
98338	Email		crowdadmin@mousoft.co.kr	65547
98339	Display name		crowd administrator	65547
98340	Active		true	65547
98341	External id		6bb6a49d-db17-44cd-a6b5-3b45bc5943c4	65547
98342	Password		*****	65547
98343	Name		crowd-administrators	65548
98344	Local		false	65548
98345	Active		true	65548
98346	Type		CROWD	65550
98347	Name		crowd	65550
98348	Created Date		2018-08-09T05:42:52.156+0000	65550
98349	Description		Crowd console	65550
98350	Attribute: atlassian_sha1_applied		true	65550
98351	Is permanent		true	65550
98352	Active		true	65550
98353	Password		*****	65550
98354	Attribute: aggregateMemberships		false	65551
98355	Attribute: aliasingEnabled		false	65551
98356	Attribute: lowerCaseOutput		false	65551
98357	com.sun.jndi.ldap.connect.pool.initsize		1	65552
98358	com.sun.jndi.ldap.connect.pool.prefsize		10	65553
98359	com.sun.jndi.ldap.connect.pool.maxsize		0	65554
98360	com.sun.jndi.ldap.connect.pool.timeout		30000	65555
98361	com.sun.jndi.ldap.connect.pool.protocol		plain ssl	65556
98362	com.sun.jndi.ldap.connect.pool.authentication		simple	65557
98363	Attribute: aggregateMemberships		false	65558
98364	Attribute: aliasingEnabled		false	65558
98365	Attribute: lowerCaseOutput		false	65558
98366	mailserver.timeout		60	65559
458753	build.number		952	425985
\.


--
-- Data for Name: cwd_cluster_heartbeat; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_cluster_heartbeat (node_id, node_name, hearbeat_timestamp) FROM stdin;
\.


--
-- Data for Name: cwd_cluster_info; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_cluster_info (node_id, ip_address, hostname, current_heap, max_heap, load_average, uptime, info_timestamp) FROM stdin;
\.


--
-- Data for Name: cwd_cluster_job; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_cluster_job (id, runner_key, job_interval, cron_expression, time_zone, next_run_timestamp, version, job_parameters, claim_node_id, claim_timestamp) FROM stdin;
CompatibilityPluginScheduler.JobId.Service Provider Session Remover	CompatibilityPluginScheduler.JobRunnerKey.Service Provider Session Remover	28800000	\N	\N	1533822175510	1	\N	\N	\N
auditLogPrunerJob	auditLogPrunerJob	\N	0 0 0 */1 * ?	\N	1533859200000	1	\N	\N	\N
AutomatedBackup	AutomatedBackup	\N	0 0 2 * * ?	\N	1533866400000	1	\N	\N	\N
licenseResourceTrigger	LicenseResourceJob	\N	0 0 */6 * * ?	\N	1533794400000	1	\N	\N	\N
sessionTokenReaperTrigger	SessionTokenReaperJob	\N	0 0 * * * ?	\N	1533794400000	1	\N	\N	\N
com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorRefresherStarter-job	com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorRefresherJob-runner	120000	\N	\N	1533793495731	2	\N	NOT_CLUSTERED	1533793375733
clusterMessageReaperTrigger	clusterMessageReaperJob	\N	0 0 */6 * * ?	\N	1533794400000	1	\N	\N	\N
userTokenReaperTrigger	UserTokenReaperJob	\N	0 0 * * * ?	\N	1533794400000	1	\N	\N	\N
tombstoneReaperTrigger	TombstoneReaperJob	\N	0 0 */6 * * ?	\N	1533794400000	1	\N	\N	\N
clusterNodeInformationPrunerJob	clusterNodeInformationPrunerJob	\N	0 0 3 */1 * ?	\N	1533870000000	1	\N	\N	\N
com.atlassian.crowd.analytics.statistics.StatisticsCollectionScheduler-job	com.atlassian.crowd.analytics.statistics.StatisticsCollectionScheduler	\N	0 0 23 * * ?	\N	1533855600000	1	\N	\N	\N
stalledSynchronisationsHandlerJob	stalledSynchronisationsHandlerJob	\N	0 */5 * * * ?	\N	1533793500000	1	\N	\N	\N
\.


--
-- Data for Name: cwd_cluster_lock; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_cluster_lock (lock_name, lock_timestamp, node_id) FROM stdin;
\.


--
-- Data for Name: cwd_cluster_message; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_cluster_message (id, channel, msg_text, msg_timestamp, sender_node_id) FROM stdin;
\.


--
-- Data for Name: cwd_cluster_message_id; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_cluster_message_id (next_val) FROM stdin;
1
\.


--
-- Data for Name: cwd_cluster_safety; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_cluster_safety (entry_key, entry_value, node_id, ip_address, entry_timestamp) FROM stdin;
\.


--
-- Data for Name: cwd_databasechangelog; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
CWD-4119	crowd	liquibase/crowd_3_1_0/01_setup_hibernate_unique_key.xml	2018-08-09 05:41:14.752089	1	EXECUTED	7:2ae7622d51cec142fab15fa76c905772	createTable tableName=hibernate_unique_key; insert tableName=hibernate_unique_key	Creates and seeds the hibernate_unique_key table used by Hibernate id generators.	\N	3.5.3	\N	\N	3793274706
CWD-5004	crowd	liquibase/crowd_3_1_0/02_denormalize_granted_permissions.xml	2018-08-09 05:41:14.772919	2	MARK_RAN	7:4646d4d053e2f11cf0e96408c4e1e72a	addColumn tableName=cwd_granted_perm; addNotNullConstraint columnName=group_name, tableName=cwd_granted_perm; addNotNullConstraint columnName=app_dir_mapping_id, tableName=cwd_granted_perm; addForeignKeyConstraint baseTableName=cwd_granted_perm, c...	Replaces cwd_granted_perm with group_name and app_dir_mapping_id in cwd_granted_perm	\N	3.5.3	\N	\N	3793274706
KRAK-1087	crowd	liquibase/crowd_3_1_3/01_audit_entry_cascade.xml	2018-08-09 05:41:14.788196	3	MARK_RAN	7:52cc69699f2b0dd6b0b86ec725cecba5	dropAllForeignKeyConstraints baseTableName=cwd_audit_log_entry; addForeignKeyConstraint baseTableName=cwd_audit_log_entry, constraintName=fk_entry_changeset, referencedTableName=cwd_audit_log_changeset	Replaces the current foreign key mapping between the changeset and the entry with one that has an ondelete cascade	\N	3.5.3	\N	\N	3793274706
KRAK-1074	crowd	liquibase/crowd_3_2_0/01_create_audit_log_entity_table.xml	2018-08-09 05:41:14.803317	4	MARK_RAN	7:9b93f7b005135067e3f0a8c8fc4184cc	createTable tableName=cwd_audit_log_entity; dropIndex indexName=idx_audit_enttype, tableName=cwd_audit_log_changeset; dropIndex indexName=idx_audit_entid, tableName=cwd_audit_log_changeset; dropIndex indexName=idx_audit_entname, tableName=cwd_audi...	Adds the audit log entity table	\N	3.5.3	\N	\N	3793274706
KRAK-1073	crowd	liquibase/crowd_3_2_0/02_audit_log_migration.xml	2018-08-09 05:41:14.810411	5	MARK_RAN	7:1d5a661f251faa52855237b68fcf0b8e	sql; sql; dropColumn columnName=entity_id, tableName=cwd_audit_log_changeset; dropColumn columnName=entity_name, tableName=cwd_audit_log_changeset; dropColumn columnName=entity_type, tableName=cwd_audit_log_changeset	Migrates the data from the audit log changeset table to the new entity table, drops the entity columns from the changeset	\N	3.5.3	\N	\N	3793274706
KRAK-1170	crowd	liquibase/crowd_3_2_0/03_audit_log_event_source.xml	2018-08-09 05:41:14.825346	6	MARK_RAN	7:0d5df47adb0ebcabf4ceb13079052e73	addColumn tableName=cwd_audit_log_changeset; addNotNullConstraint columnName=event_source, tableName=cwd_audit_log_changeset; createIndex indexName=idx_audit_source, tableName=cwd_audit_log_changeset	Creates the source column for the cwd_audit_log_changeset table	\N	3.5.3	\N	\N	3793274706
\.


--
-- Data for Name: cwd_databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: cwd_directory; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_directory (id, directory_name, lower_directory_name, created_date, updated_date, active, description, impl_class, lower_impl_class, directory_type) FROM stdin;
163841	MOUSoft Inc. Crowd server	mousoft inc. crowd server	2018-08-09 05:42:30.117	2018-08-09 05:42:55.281	T		com.atlassian.crowd.directory.InternalDirectory	com.atlassian.crowd.directory.internaldirectory	INTERNAL
\.


--
-- Data for Name: cwd_directory_attribute; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_directory_attribute (directory_id, attribute_name, attribute_value) FROM stdin;
163841	password_complexity_message	
163841	user_encryption_method	atlassian-security
163841	password_max_attempts	0
163841	password_history_count	0
163841	password_regex	
163841	password_max_change_time	0
163841	configuration.change.timestamp	1533793375071
\.


--
-- Data for Name: cwd_directory_operation; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_directory_operation (directory_id, operation_type) FROM stdin;
163841	CREATE_USER
163841	CREATE_GROUP
163841	UPDATE_USER
163841	UPDATE_GROUP
163841	UPDATE_USER_ATTRIBUTE
163841	UPDATE_GROUP_ATTRIBUTE
163841	DELETE_USER
163841	DELETE_GROUP
\.


--
-- Data for Name: cwd_expirable_user_token; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_expirable_user_token (id, token, user_name, email_address, expiry_date, directory_id) FROM stdin;
\.


--
-- Data for Name: cwd_granted_perm; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_granted_perm (id, created_date, permission_id, app_dir_mapping_id, group_name) FROM stdin;
393217	2018-08-09 05:42:52.187	2	327681	crowd-administrators
393218	2018-08-09 05:42:55.328	2	327681	crowd-administrators
\.


--
-- Data for Name: cwd_group; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id, external_id) FROM stdin;
262145	crowd-administrators	crowd-administrators	T	F	2018-08-09 05:42:52.112	2018-08-09 05:42:52.112	\N	GROUP	163841	\N
\.


--
-- Data for Name: cwd_group_attribute; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_group_attribute (id, group_id, directory_id, attribute_name, attribute_value, attribute_lower_value) FROM stdin;
\.


--
-- Data for Name: cwd_membership; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, created_date, directory_id) FROM stdin;
294913	262145	196609	GROUP_USER	GROUP	crowd-administrators	crowd-administrators	crowdadmin	crowdadmin	2018-08-09 05:42:52.133	163841
\.


--
-- Data for Name: cwd_property; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_property (property_key, property_name, property_value) FROM stdin;
crowd	deployment.title	MOUSoft Inc. Crowd server
crowd	des.encryption.key	l+qDQDj4Kgs=
crowd	session.time	1800000
crowd	cache.enabled	true
crowd	base.url	http://localhost:8099/crowd
crowd	mailserver.message.template	Hello $firstname $lastname,\n\nYou (or someone else) have requested to reset your password for $deploymenttitle on $date.\n\nIf you follow the link below you will be able to personally reset your password.\n$resetlink\n\nThis password reset request is valid for the next 24 hours.\n\nHere are the details of your account:\n\nUsername: $username\nFull name: $firstname $lastname\nYour account is currently: $active\n\n$deploymenttitle administrator
crowd	email.template.forgotten.username	Hello $firstname $lastname,\n\nYou have requested the username for your email address: $email.\n\nYour username(s) are: $username\n\nIf you think this email was sent incorrectly, please contact one of the administrators at: $admincontact\n\n$deploymenttitle administrator
crowd	mailserver.prefix	[MOUSoft Inc. Crowd server - Atlassian Crowd]
crowd	current.license.resource.total	0
crowd	secure.cookie	false
crowd	com.sun.jndi.ldap.connect.pool.initsize	1
crowd	com.sun.jndi.ldap.connect.pool.prefsize	10
crowd	com.sun.jndi.ldap.connect.pool.maxsize	0
crowd	com.sun.jndi.ldap.connect.pool.timeout	30000
crowd	com.sun.jndi.ldap.connect.pool.protocol	plain ssl
crowd	com.sun.jndi.ldap.connect.pool.authentication	simple
crowd	mailserver.timeout	60
crowd	build.number	952
plugin.null	com.atlassian.analytics.client.configuration.uuid	0c326d11-e450-4a29-b1c3-a3a50e63c975
plugin.null	com.atlassian.analytics.client.configuration.serverid	
plugin.null	com.atlassian.analytics.client.configuration..policy_acknowledged	true
plugin.null	com.atlassian.analytics.client.configuration..analytics_enabled	true
plugin.null	crowd-saml-plugin:build	1
plugin.null	crowd.analytics.instance.startup.info	#java.util.Map\nstart_time\f1533793375787\nbuild_number\f952\nclustered\ffalse
plugin.null	com.atlassian.analytics.client.configuration..logged_base_analytics_data	true
plugin.null	com.atlassian.nps.plugin.status.nps_acknowledged	true
plugin.null	com.atlassian.nps.plugin.status.nps_enabled	false
\.


--
-- Data for Name: cwd_synchronisation_status; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_synchronisation_status (id, directory_id, node_id, sync_start, sync_end, sync_status, status_parameters) FROM stdin;
\.


--
-- Data for Name: cwd_synchronisation_token; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_synchronisation_token (directory_id, sync_status_token) FROM stdin;
\.


--
-- Data for Name: cwd_token; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_token (id, directory_id, entity_name, random_number, identifier_hash, random_hash, created_date, last_accessed_date, last_accessed_time, duration) FROM stdin;
\.


--
-- Data for Name: cwd_tombstone; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_tombstone (id, tombstone_type, tombstone_timestamp, entity_name, directory_id, parent, application_id) FROM stdin;
\.


--
-- Data for Name: cwd_user; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, external_id, directory_id, credential) FROM stdin;
196609	crowdadmin	crowdadmin	T	2018-08-09 05:42:52.059	2018-08-09 05:42:52.059	crowd	crowd	administrator	administrator	crowd administrator	crowd administrator	crowdadmin@mousoft.co.kr	crowdadmin@mousoft.co.kr	6bb6a49d-db17-44cd-a6b5-3b45bc5943c4	163841	{PKCS5S2}kpimrebLQcvjQl9odg2sAarDe0LhLZWB+as0k18C5qRZNVPJdbw6xUHFyujzAnI+
\.


--
-- Data for Name: cwd_user_attribute; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) FROM stdin;
229377	196609	163841	requiresPasswordChange	false	false
229378	196609	163841	invalidPasswordAttempts	0	0
229379	196609	163841	passwordLastChanged	1533793372078	1533793372078
491521	196609	163841	lastAuthenticated	1533793440750	1533793440750
491522	196609	163841	lastActive	1533793441107	1533793441107
\.


--
-- Data for Name: cwd_user_credential_record; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_user_credential_record (id, user_id, password_hash, list_index) FROM stdin;
\.


--
-- Data for Name: cwd_webhook; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.cwd_webhook (id, endpoint_url, application_id, token, oldest_failure_date, failures_since_last_success) FROM stdin;
\.


--
-- Data for Name: hibernate_unique_key; Type: TABLE DATA; Schema: public; Owner: crowduser
--

COPY public.hibernate_unique_key (next_hi) FROM stdin;
17
\.


--
-- Name: cwd_app_dir_default_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_default_groups
    ADD CONSTRAINT cwd_app_dir_default_groups_pkey PRIMARY KEY (id);


--
-- Name: cwd_app_dir_group_mapping_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_group_mapping
    ADD CONSTRAINT cwd_app_dir_group_mapping_pkey PRIMARY KEY (id);


--
-- Name: cwd_app_dir_mapping_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_mapping
    ADD CONSTRAINT cwd_app_dir_mapping_pkey PRIMARY KEY (id);


--
-- Name: cwd_app_dir_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_operation
    ADD CONSTRAINT cwd_app_dir_operation_pkey PRIMARY KEY (app_dir_mapping_id, operation_type);


--
-- Name: cwd_application_address_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_address
    ADD CONSTRAINT cwd_application_address_pkey PRIMARY KEY (application_id, remote_address);


--
-- Name: cwd_application_alias_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_alias
    ADD CONSTRAINT cwd_application_alias_pkey PRIMARY KEY (id);


--
-- Name: cwd_application_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_attribute
    ADD CONSTRAINT cwd_application_attribute_pkey PRIMARY KEY (application_id, attribute_name);


--
-- Name: cwd_application_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application
    ADD CONSTRAINT cwd_application_pkey PRIMARY KEY (id);


--
-- Name: cwd_audit_log_changeset_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_audit_log_changeset
    ADD CONSTRAINT cwd_audit_log_changeset_pkey PRIMARY KEY (id);


--
-- Name: cwd_audit_log_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_audit_log_entity
    ADD CONSTRAINT cwd_audit_log_entity_pkey PRIMARY KEY (id);


--
-- Name: cwd_audit_log_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_audit_log_entry
    ADD CONSTRAINT cwd_audit_log_entry_pkey PRIMARY KEY (id);


--
-- Name: cwd_cluster_heartbeat_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_cluster_heartbeat
    ADD CONSTRAINT cwd_cluster_heartbeat_pkey PRIMARY KEY (node_id);


--
-- Name: cwd_cluster_info_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_cluster_info
    ADD CONSTRAINT cwd_cluster_info_pkey PRIMARY KEY (node_id);


--
-- Name: cwd_cluster_job_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_cluster_job
    ADD CONSTRAINT cwd_cluster_job_pkey PRIMARY KEY (id);


--
-- Name: cwd_cluster_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_cluster_lock
    ADD CONSTRAINT cwd_cluster_lock_pkey PRIMARY KEY (lock_name);


--
-- Name: cwd_cluster_message_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_cluster_message
    ADD CONSTRAINT cwd_cluster_message_pkey PRIMARY KEY (id);


--
-- Name: cwd_cluster_safety_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_cluster_safety
    ADD CONSTRAINT cwd_cluster_safety_pkey PRIMARY KEY (entry_key);


--
-- Name: cwd_directory_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_directory_attribute
    ADD CONSTRAINT cwd_directory_attribute_pkey PRIMARY KEY (directory_id, attribute_name);


--
-- Name: cwd_directory_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_directory_operation
    ADD CONSTRAINT cwd_directory_operation_pkey PRIMARY KEY (directory_id, operation_type);


--
-- Name: cwd_directory_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_directory
    ADD CONSTRAINT cwd_directory_pkey PRIMARY KEY (id);


--
-- Name: cwd_expirable_user_token_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_expirable_user_token
    ADD CONSTRAINT cwd_expirable_user_token_pkey PRIMARY KEY (id);


--
-- Name: cwd_granted_perm_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_granted_perm
    ADD CONSTRAINT cwd_granted_perm_pkey PRIMARY KEY (id);


--
-- Name: cwd_group_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_group_attribute
    ADD CONSTRAINT cwd_group_attribute_pkey PRIMARY KEY (id);


--
-- Name: cwd_group_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_group
    ADD CONSTRAINT cwd_group_pkey PRIMARY KEY (id);


--
-- Name: cwd_membership_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_membership
    ADD CONSTRAINT cwd_membership_pkey PRIMARY KEY (id);


--
-- Name: cwd_property_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_property
    ADD CONSTRAINT cwd_property_pkey PRIMARY KEY (property_key, property_name);


--
-- Name: cwd_synchronisation_status_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_synchronisation_status
    ADD CONSTRAINT cwd_synchronisation_status_pkey PRIMARY KEY (id);


--
-- Name: cwd_synchronisation_token_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_synchronisation_token
    ADD CONSTRAINT cwd_synchronisation_token_pkey PRIMARY KEY (directory_id);


--
-- Name: cwd_token_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_token
    ADD CONSTRAINT cwd_token_pkey PRIMARY KEY (id);


--
-- Name: cwd_tombstone_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_tombstone
    ADD CONSTRAINT cwd_tombstone_pkey PRIMARY KEY (id);


--
-- Name: cwd_user_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user_attribute
    ADD CONSTRAINT cwd_user_attribute_pkey PRIMARY KEY (id);


--
-- Name: cwd_user_credential_record_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user_credential_record
    ADD CONSTRAINT cwd_user_credential_record_pkey PRIMARY KEY (id);


--
-- Name: cwd_user_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user
    ADD CONSTRAINT cwd_user_pkey PRIMARY KEY (id);


--
-- Name: cwd_webhook_pkey; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_webhook
    ADD CONSTRAINT cwd_webhook_pkey PRIMARY KEY (id);


--
-- Name: pk_cwd_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_databasechangeloglock
    ADD CONSTRAINT pk_cwd_databasechangeloglock PRIMARY KEY (id);


--
-- Name: uk_alias_app_l_alias; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_alias
    ADD CONSTRAINT uk_alias_app_l_alias UNIQUE (application_id, lower_alias_name);


--
-- Name: uk_alias_app_l_username; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_alias
    ADD CONSTRAINT uk_alias_app_l_username UNIQUE (application_id, lower_user_name);


--
-- Name: uk_app_dir; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_mapping
    ADD CONSTRAINT uk_app_dir UNIQUE (application_id, directory_id);


--
-- Name: uk_app_dir_group; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_group_mapping
    ADD CONSTRAINT uk_app_dir_group UNIQUE (app_dir_mapping_id, group_name);


--
-- Name: uk_app_l_name; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application
    ADD CONSTRAINT uk_app_l_name UNIQUE (lower_application_name);


--
-- Name: uk_byovf64sr5h7khn3h716lavpn; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_default_groups
    ADD CONSTRAINT uk_byovf64sr5h7khn3h716lavpn UNIQUE (application_mapping_id, group_name);


--
-- Name: uk_dir_l_name; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_directory
    ADD CONSTRAINT uk_dir_l_name UNIQUE (lower_directory_name);


--
-- Name: uk_expirable_user_token; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_expirable_user_token
    ADD CONSTRAINT uk_expirable_user_token UNIQUE (token);


--
-- Name: uk_group_name_attr_lval; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_group_attribute
    ADD CONSTRAINT uk_group_name_attr_lval UNIQUE (group_id, attribute_name, attribute_lower_value);


--
-- Name: uk_group_name_dir_id; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_group
    ADD CONSTRAINT uk_group_name_dir_id UNIQUE (lower_group_name, directory_id);


--
-- Name: uk_mem_parent_child_type; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_membership
    ADD CONSTRAINT uk_mem_parent_child_type UNIQUE (parent_id, child_id, membership_type);


--
-- Name: uk_token_id_hash; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_token
    ADD CONSTRAINT uk_token_id_hash UNIQUE (identifier_hash);


--
-- Name: uk_user_attr_name_lval; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user_attribute
    ADD CONSTRAINT uk_user_attr_name_lval UNIQUE (user_id, attribute_name, attribute_lower_value);


--
-- Name: uk_user_name_dir_id; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user
    ADD CONSTRAINT uk_user_name_dir_id UNIQUE (lower_user_name, directory_id);


--
-- Name: uk_webhook_url_app; Type: CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_webhook
    ADD CONSTRAINT uk_webhook_url_app UNIQUE (endpoint_url, application_id);


--
-- Name: idx_app_active; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_app_active ON public.cwd_application USING btree (active);


--
-- Name: idx_app_dir_group_group_dir; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_app_dir_group_group_dir ON public.cwd_app_dir_group_mapping USING btree (directory_id, group_name);


--
-- Name: idx_app_type; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_app_type ON public.cwd_application USING btree (application_type);


--
-- Name: idx_audit_authid; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_authid ON public.cwd_audit_log_changeset USING btree (author_id);


--
-- Name: idx_audit_authname; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_authname ON public.cwd_audit_log_changeset USING btree (author_name);


--
-- Name: idx_audit_authtype; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_authtype ON public.cwd_audit_log_changeset USING btree (author_type);


--
-- Name: idx_audit_entid; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_entid ON public.cwd_audit_log_entity USING btree (entity_id);


--
-- Name: idx_audit_entname; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_entname ON public.cwd_audit_log_entity USING btree (entity_name);


--
-- Name: idx_audit_enttype_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_enttype_id ON public.cwd_audit_log_entity USING btree (entity_type, entity_id);


--
-- Name: idx_audit_enttype_name; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_enttype_name ON public.cwd_audit_log_entity USING btree (entity_type, entity_name);


--
-- Name: idx_audit_ip; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_ip ON public.cwd_audit_log_changeset USING btree (ip_address);


--
-- Name: idx_audit_propname; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_propname ON public.cwd_audit_log_entry USING btree (property_name);


--
-- Name: idx_audit_source; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_source ON public.cwd_audit_log_changeset USING btree (event_source);


--
-- Name: idx_audit_timestamp; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_audit_timestamp ON public.cwd_audit_log_changeset USING btree (audit_timestamp);


--
-- Name: idx_auth_eventtype; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_auth_eventtype ON public.cwd_audit_log_changeset USING btree (event_type);


--
-- Name: idx_changeset_entity; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_changeset_entity ON public.cwd_audit_log_entity USING btree (changeset_id);


--
-- Name: idx_dir_active; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_dir_active ON public.cwd_directory USING btree (active);


--
-- Name: idx_dir_l_impl_class; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_dir_l_impl_class ON public.cwd_directory USING btree (lower_impl_class);


--
-- Name: idx_dir_type; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_dir_type ON public.cwd_directory USING btree (directory_type);


--
-- Name: idx_directory_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_directory_id ON public.cwd_synchronisation_status USING btree (directory_id);


--
-- Name: idx_entry_changeset; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_entry_changeset ON public.cwd_audit_log_entry USING btree (changeset_id);


--
-- Name: idx_expirable_user_token_key; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_expirable_user_token_key ON public.cwd_expirable_user_token USING btree (token);


--
-- Name: idx_external_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_external_id ON public.cwd_user USING btree (external_id);


--
-- Name: idx_group_active; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_group_active ON public.cwd_group USING btree (active, directory_id);


--
-- Name: idx_group_attr_dir_name_lval; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_group_attr_dir_name_lval ON public.cwd_group_attribute USING btree (directory_id, attribute_name, attribute_lower_value);


--
-- Name: idx_group_attr_group_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_group_attr_group_id ON public.cwd_group_attribute USING btree (group_id);


--
-- Name: idx_group_dir_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_group_dir_id ON public.cwd_group USING btree (directory_id);


--
-- Name: idx_group_external_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_group_external_id ON public.cwd_group USING btree (external_id);


--
-- Name: idx_hb_timestamp; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_hb_timestamp ON public.cwd_cluster_heartbeat USING btree (hearbeat_timestamp);


--
-- Name: idx_mem_dir_child; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_mem_dir_child ON public.cwd_membership USING btree (membership_type, lower_child_name, directory_id);


--
-- Name: idx_mem_dir_parent; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_mem_dir_parent ON public.cwd_membership USING btree (membership_type, lower_parent_name, directory_id);


--
-- Name: idx_mem_dir_parent_child; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_mem_dir_parent_child ON public.cwd_membership USING btree (membership_type, lower_parent_name, lower_child_name, directory_id);


--
-- Name: idx_sync_end; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_sync_end ON public.cwd_synchronisation_status USING btree (sync_end);


--
-- Name: idx_sync_status_node_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_sync_status_node_id ON public.cwd_synchronisation_status USING btree (node_id);


--
-- Name: idx_token_dir_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_token_dir_id ON public.cwd_token USING btree (directory_id);


--
-- Name: idx_token_key; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_token_key ON public.cwd_token USING btree (random_hash);


--
-- Name: idx_token_last_access; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_token_last_access ON public.cwd_token USING btree (last_accessed_date);


--
-- Name: idx_token_name_dir_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_token_name_dir_id ON public.cwd_token USING btree (directory_id, entity_name);


--
-- Name: idx_tombstone_type_timestamp; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_tombstone_type_timestamp ON public.cwd_tombstone USING btree (tombstone_type, tombstone_timestamp);


--
-- Name: idx_user_active; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_active ON public.cwd_user USING btree (active, directory_id);


--
-- Name: idx_user_attr_dir_name_lval; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_attr_dir_name_lval ON public.cwd_user_attribute USING btree (directory_id, attribute_name, attribute_lower_value);


--
-- Name: idx_user_attr_user_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_attr_user_id ON public.cwd_user_attribute USING btree (user_id);


--
-- Name: idx_user_lower_display_name; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_lower_display_name ON public.cwd_user USING btree (lower_display_name, directory_id);


--
-- Name: idx_user_lower_email_address; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_lower_email_address ON public.cwd_user USING btree (lower_email_address, directory_id);


--
-- Name: idx_user_lower_first_name; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_lower_first_name ON public.cwd_user USING btree (lower_first_name, directory_id);


--
-- Name: idx_user_lower_last_name; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_lower_last_name ON public.cwd_user USING btree (lower_last_name, directory_id);


--
-- Name: idx_user_name_dir_id; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_user_name_dir_id ON public.cwd_user USING btree (directory_id);


--
-- Name: idx_webhook_url_app; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX idx_webhook_url_app ON public.cwd_webhook USING btree (endpoint_url, application_id);


--
-- Name: nextruntime_idx; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX nextruntime_idx ON public.cwd_cluster_job USING btree (next_run_timestamp);


--
-- Name: runnerkey_idx; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX runnerkey_idx ON public.cwd_cluster_job USING btree (runner_key);


--
-- Name: sender_node_id_idx; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX sender_node_id_idx ON public.cwd_cluster_message USING btree (sender_node_id);


--
-- Name: timestamp_idx; Type: INDEX; Schema: public; Owner: crowduser
--

CREATE INDEX timestamp_idx ON public.cwd_cluster_message USING btree (msg_timestamp);


--
-- Name: fk_alias_app_id; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_alias
    ADD CONSTRAINT fk_alias_app_id FOREIGN KEY (application_id) REFERENCES public.cwd_application(id);


--
-- Name: fk_app_dir_app; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_mapping
    ADD CONSTRAINT fk_app_dir_app FOREIGN KEY (application_id) REFERENCES public.cwd_application(id);


--
-- Name: fk_app_dir_dir; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_mapping
    ADD CONSTRAINT fk_app_dir_dir FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_app_dir_group_app; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_group_mapping
    ADD CONSTRAINT fk_app_dir_group_app FOREIGN KEY (application_id) REFERENCES public.cwd_application(id);


--
-- Name: fk_app_dir_group_dir; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_group_mapping
    ADD CONSTRAINT fk_app_dir_group_dir FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_app_dir_group_mapping; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_group_mapping
    ADD CONSTRAINT fk_app_dir_group_mapping FOREIGN KEY (app_dir_mapping_id) REFERENCES public.cwd_app_dir_mapping(id);


--
-- Name: fk_app_dir_mapping; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_operation
    ADD CONSTRAINT fk_app_dir_mapping FOREIGN KEY (app_dir_mapping_id) REFERENCES public.cwd_app_dir_mapping(id);


--
-- Name: fk_app_mapping; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_app_dir_default_groups
    ADD CONSTRAINT fk_app_mapping FOREIGN KEY (application_mapping_id) REFERENCES public.cwd_app_dir_mapping(id);


--
-- Name: fk_application_address; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_address
    ADD CONSTRAINT fk_application_address FOREIGN KEY (application_id) REFERENCES public.cwd_application(id);


--
-- Name: fk_application_attribute; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_application_attribute
    ADD CONSTRAINT fk_application_attribute FOREIGN KEY (application_id) REFERENCES public.cwd_application(id);


--
-- Name: fk_changeset_entity; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_audit_log_entity
    ADD CONSTRAINT fk_changeset_entity FOREIGN KEY (changeset_id) REFERENCES public.cwd_audit_log_changeset(id) ON DELETE CASCADE;


--
-- Name: fk_directory_attribute; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_directory_attribute
    ADD CONSTRAINT fk_directory_attribute FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_directory_id; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_group
    ADD CONSTRAINT fk_directory_id FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_directory_operation; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_directory_operation
    ADD CONSTRAINT fk_directory_operation FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_entry_changeset; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_audit_log_entry
    ADD CONSTRAINT fk_entry_changeset FOREIGN KEY (changeset_id) REFERENCES public.cwd_audit_log_changeset(id) ON DELETE CASCADE;


--
-- Name: fk_granted_perm_dir_mapping; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_granted_perm
    ADD CONSTRAINT fk_granted_perm_dir_mapping FOREIGN KEY (app_dir_mapping_id) REFERENCES public.cwd_app_dir_mapping(id);


--
-- Name: fk_group_attr_dir_id; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_group_attribute
    ADD CONSTRAINT fk_group_attr_dir_id FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_group_attr_id_group_id; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_group_attribute
    ADD CONSTRAINT fk_group_attr_id_group_id FOREIGN KEY (group_id) REFERENCES public.cwd_group(id);


--
-- Name: fk_membership_dir; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_membership
    ADD CONSTRAINT fk_membership_dir FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_user_attr_dir_id; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user_attribute
    ADD CONSTRAINT fk_user_attr_dir_id FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_user_attribute_id_user_id; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user_attribute
    ADD CONSTRAINT fk_user_attribute_id_user_id FOREIGN KEY (user_id) REFERENCES public.cwd_user(id);


--
-- Name: fk_user_cred_user; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user_credential_record
    ADD CONSTRAINT fk_user_cred_user FOREIGN KEY (user_id) REFERENCES public.cwd_user(id);


--
-- Name: fk_user_dir_id; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_user
    ADD CONSTRAINT fk_user_dir_id FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fk_webhook_app; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_webhook
    ADD CONSTRAINT fk_webhook_app FOREIGN KEY (application_id) REFERENCES public.cwd_application(id);


--
-- Name: fkif9xqfw310f5v63l8lswn5mvg; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_synchronisation_token
    ADD CONSTRAINT fkif9xqfw310f5v63l8lswn5mvg FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: fkk7c1i4daxljhvhxo05fkch152; Type: FK CONSTRAINT; Schema: public; Owner: crowduser
--

ALTER TABLE ONLY public.cwd_synchronisation_status
    ADD CONSTRAINT fkk7c1i4daxljhvhxo05fkch152 FOREIGN KEY (directory_id) REFERENCES public.cwd_directory(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

