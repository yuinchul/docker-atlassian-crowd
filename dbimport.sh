#!/bin/bash
whoami
#service postgresql start
sudo -u postgres bash << EOF
echo "In"
if [ "$( psql --host=localhost --port=5432 --username=postgres --no-password -tAc "SELECT 1 FROM pg_database WHERE datname='crowddb'" )" = '1' ]
then
	echo "Database already exists, So don't execute the database importing "
else
	echo "Database does not exist, So importing the database "
	psql -c "ALTER USER postgres WITH PASSWORD 'root'";
	psql -c "create database crowddb";
	psql -c "create user crowduser with password 'crowdpass'";
	psql -c "grant all privileges on database crowddb to crowduser";
	psql --host=localhost --port=5432 --username=postgres --no-password --dbname=crowddb < /var/lib/postgresql/crowddatabase_backup.sql
fi
EOF
echo "Out"
