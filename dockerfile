# VERSION 0.9
#
# Extends ubuntu-base with Atlassian JIRA installation
#
FROM ubuntu:16.04
MAINTAINER icyoo <icyoo@mousoft.co.kr>

# setting variable for the for new container
ENV TZ=Asia/Seoul
ENV CROWD_VERSION 3.2.1
ENV MAXJVM 1048m
ENV MINJVM 1048m
ENV CROWDUSER atlcrowd
ENV INSTALL_DIR /app
ENV APP_INSTALL_DIR /app/crowdZZ
ENV APP_DATA_DIR /app/data
# you can give the ipaddress or domain name
ENV IPADDRESS crowdcto.lge.com
# this port will be used to connect from client and it is defined "docker run -p HOSTPORT:WEBPORT ~~~"
ENV HOSTPORT 8099
ENV WEBPORT 8095
ENV SHUTDOWNPORT 80
ENV DBA_PASS 		root@@123
ENV CROWD_DBNAME 	crowddb2
ENV CROWD_DBUSER	crowduser2
ENV CROWD_DBPASS	crowd@@1234

# ADD
RUN mkdir -p ${INSTALL_DIR}
RUN mkdir -p ${APP_INSTALL_DIR}
RUN mkdir -p ${APP_DATA_DIR}

# This is in accordance to : https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04
RUN apt-get update && \
apt-get install -y sudo iputils-ping net-tools locales \
wget openjdk-8-jdk nmap vim upstart ssh openssh-server apache2 && \
apt-get install -y ant && \
apt-get clean && \
rm -rf /var/lib/apt/lists/* && \
rm -rf /var/cache/oracle-jdk8-installer;

#RUN apt-get update && apt-get install -y software-properties-common

# Set the locale
RUN locale-gen ko_KR.UTF-8
ENV LANG ko_KR.UTF-8
ENV LANGUAGE ko_KR:UTF-8
ENV LC_ALL ko_KR.UTF-8

# For postgresql 9.6 update
#RUN add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/$(lsb_release -sc)-pgdg main"
#RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
RUN apt-get update
RUN apt-get install -y postgresql

# Fix certificate issues, found as of 
# https://bugs.launchpad.net/ubuntu/+source/ca-certificates-java/+bug/983302
RUN apt-get update && \
apt-get install -y ca-certificates-java && \
apt-get clean && \
update-ca-certificates -f && \
rm -rf /var/lib/apt/lists/* && \
rm -rf /var/cache/oracle-jdk8-installer;

# Setup JAVA_HOME, this is useful for docker commandline
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME

RUN useradd -c "Atlassian Crowd" -u 1001 -b /home/ -m ${CROWDUSER}
RUN chown -R ${CROWDUSER}:${CROWDUSER} ${INSTALL_DIR}

RUN wget -P ${INSTALL_DIR}/ https://www.atlassian.com/software/crowd/downloads/binary/atlassian-crowd-${CROWD_VERSION}.tar.gz
RUN chown ${CROWDUSER}:${CROWDUSER} ${INSTALL_DIR}/atlassian-crowd-${CROWD_VERSION}.tar.gz

# file extracting the crowd.tar.gz
RUN mv ${INSTALL_DIR}/atlassian-crowd-${CROWD_VERSION}.tar.gz ${APP_INSTALL_DIR}/.
RUN cd ${APP_INSTALL_DIR}/
RUN ls -al ${APP_INSTALL_DIR}/
RUN tar xvf ${APP_INSTALL_DIR}/atlassian-crowd-${CROWD_VERSION}.tar.gz
RUN mv /atlassian-crowd-${CROWD_VERSION} ${APP_INSTALL_DIR}/.

# posgresql backup file import 
# USER postgres
RUN cd /etc/postgresql/9.5/main/
ADD postgresql.conf /etc/postgresql/9.5/main/postgresql.conf
RUN chown postgres:postgres /etc/postgresql/9.5/main/postgresql.conf
RUN chmod 644 /etc/postgresql/9.5/main/postgresql.conf
ADD pg_hba.conf /etc/postgresql/9.5/main/pg_hba.conf
RUN chown postgres:postgres /etc/postgresql/9.5/main/pg_hba.conf
RUN chmod 644 /etc/postgresql/9.5/main/pg_hba.conf
ADD .pgpass /var/lib/postgresql/.pgpass
RUN chown postgres:postgres /var/lib/postgresql/.pgpass
RUN chmod 600 /var/lib/postgresql/.pgpass
RUN sed -i 's/root/'${DBA_PASS}'/g' /var/lib/postgresql/.pgpass
ADD crowddatabase_backup.sql /var/lib/postgresql/crowddatabase_backup.sql
RUN chown postgres:postgres /var/lib/postgresql/crowddatabase_backup.sql
RUN chmod 777 /var/lib/postgresql/crowddatabase_backup.sql
ADD dbimport.sh /var/lib/postgresql/dbimport.sh
RUN chown postgres:postgres /var/lib/postgresql/dbimport.sh
RUN chmod 777 /var/lib/postgresql/dbimport.sh 
RUN cp /var/lib/postgresql/.pgpass ~/.

# dbchecking and setup
RUN sed -i 's/root/'${DBA_PASS}'/g' /var/lib/postgresql/dbimport.sh
RUN sed -i 's/crowddb/'${CROWD_DBNAME}'/g' /var/lib/postgresql/dbimport.sh
RUN sed -i 's/crowduser/'${CROWD_DBUSER}'/g' /var/lib/postgresql/dbimport.sh
RUN sed -i 's/crowduser/'${CROWD_DBUSER}'/g' /var/lib/postgresql/crowddatabase_backup.sql
RUN sed -i 's/crowdpass/'${CROWD_DBPASS}'/g' /var/lib/postgresql/dbimport.sh

RUN chmod 744 /var/lib/postgresql/dbimport.sh

# Crowd Data Folder setting
ADD crowd/ ${APP_DATA_DIR}/crowd

# change the datafolder location to crowd-initpropertislfl
RUN echo crowd.home=${APP_DATA_DIR}/crowd/ >> ${APP_INSTALL_DIR}/atlassian-crowd-${CROWD_VERSION}/crowd-webapp/WEB-INF/classes/crowd-init.properties 
RUN sed -i 's/crowddb/'${CROWD_DBNAME}'/g' ${APP_DATA_DIR}/crowd/shared/crowd.cfg.xml
RUN sed -i 's/crowduser/'${CROWD_DBUSER}'/g' ${APP_DATA_DIR}/crowd/shared/crowd.cfg.xml
RUN sed -i 's/crowdpass/'${CROWD_DBPASS}'/g' ${APP_DATA_DIR}/crowd/shared/crowd.cfg.xml
#RUN sed -i 's/crowddb/'${CROWD_DBNAME}'/g' /app/data/crowd/shared/crowd.cfg.xml
#RUN sed -i 's/crowduser/'${CROWD_DBUSER}'/g' /app/data/crowd/shared/crowd.cfg.xml
#RUN sed -i 's/crowdpass/"${CROWD_DBPASS}"/g' /app/data/crowd/shared/crowd.cfg.xml

# change ownership
RUN chown -R ${CROWDUSER}:${CROWDUSER} ${INSTALL_DIR} 
RUN chown -R ${CROWDUSER}:${CROWDUSER} ${APP_INSTALL_DIR}
RUN chown -R ${CROWDUSER}:${CROWDUSER} ${APP_DATA_DIR}

# EXPOSE
EXPOSE ${WEBPORT}
EXPOSE ${SHUTDOWNPORT}
EXPOSE 5432

# VOLUMES 
VOLUME ${APP_DATA_DIR}
VOLUME ["/var/lib/postgresql"]
VOLUME ["/var/log/postgresql"]

# for entry execution processes.
COPY "docker-entrypoint.sh" "/"
RUN sed -i 's#\/home\/atlcrowd#'"${APP_INSTALL_DIR}"/atlassian-crowd-"${CROWD_VERSION}"/'#g' /docker-entrypoint.sh 
RUN chmod 744 /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

# CMD ["-fg","colpack2"]

