#!/bin/bash
whoami
set -e
service postgresql start
/var/lib/postgresql/dbimport.sh
/home/atlcrowd/start_crowd.sh
tail -f /dev/null
exec "$@"
