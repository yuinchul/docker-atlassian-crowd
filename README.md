# docker-atlassian-crowd Creating Guideline

## Introduction

### 실습 전 준비사항

다양한 환경에서 실습을 진행할 경우 예상하지 못한 문제가 발생하는 경우가 많아 가급적 동일한 환경을 사용합니다. AWS를 사용하며 비용은 8시간 기준 500원 정도가 발생합니다.

- [Workshop 개발 환경 셋팅하기](https://github.com/subicura/workshop-init)

## 가이드

- [Guide 01 - Docker](./guide/guide-01.md)
  - [Task 1. Docker 기본 실습](./guide/guide-01.md#task-1-docker-기본-실습)
- [Guide 02 - Docker Compose](./guide/guide-02.md)
  - [Task 1. Docker Compose 기본 실습](./guide/guide-02.md#task-1-docker-compose-기본-실습)
- [Guide 03 - Kubernetes](./guide/guide-03.md)
  - [Task 1. kubectl](./guide/guide-03/task-01.md)
  - [Task 2. pod](./guide/guide-03/task-02.md)
  - [Task 3. replicaset](./guide/guide-03/task-03.md)
  - [Task 4. deployment](./guide/guide-03/task-04.md)
  - [Task 5. service](./guide/guide-03/task-05.md)
  - [Task 6. load balancer](./guide/guide-03/task-06.md)
  - [Task 7. ingress](./guide/guide-03/task-07.md)
  - [Bonus. Horizontal Pod Autoscaler](./guide/guide-03/bonus.md)

## 문의 & 궁금한 사항

페이스북 메신저 또는 트위터로 연락주세요.

- https://www.facebook.com/subicura
- https://twitter.com/subicura
- subicura(at)subicura(dot)com
